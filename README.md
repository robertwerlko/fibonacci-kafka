# Find nth Fibonacci number, Used Kafka for queue

## Up docker image using docker

```shell
docker-compose up -d
```

## Install requirements

```shell
pip install -r requirements.txt
```

## Run project

```shell
python main.py
```