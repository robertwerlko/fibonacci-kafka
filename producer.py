from kafka import KafkaProducer
import settings

producer = KafkaProducer(
    bootstrap_servers=settings.bootstrap_servers
)
