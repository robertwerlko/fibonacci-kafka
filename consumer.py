from kafka import KafkaConsumer
import settings

topicName = settings.KAFKA_TOPIC_NAME
consumer = KafkaConsumer(topicName, group_id=settings.KAFKA_GROUP_ID, bootstrap_servers=settings.bootstrap_servers)
