KAFKA_HOST = 'localhost'
KAFKA_PORT = 29092
KAFKA_TOPIC_NAME = 'fibonacci'
KAFKA_GROUP_ID = 'group1'

bootstrap_servers = [f'{KAFKA_HOST}:{KAFKA_PORT}']
