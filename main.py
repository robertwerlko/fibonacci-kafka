import json

import settings
from producer import producer
from consumer import consumer


def fibonacci(n):
    f = [0, 1]
    for i in range(2, n + 1):
        f.append(f[i - 1] + f[i - 2])
    return f


for msg in consumer:
    data = json.loads(msg.value)
    break


def detect_fib_numbers_for_users():
    n = int(input(
        """Function for nth fibonacci. -1 is point of system exit. 
        Enter n(n<=1000) system return nth Fibonacci number\n"""
    ))
    if n == -1:
        return

    print(f"{n} th fibonacci number is {data['fibonacci'][n]}\n")
    return detect_fib_numbers_for_users()


def main():
    fibonacci_numbers = fibonacci(1000)
    json_string = json.dumps({'fibonacci': fibonacci_numbers}).encode()

    producer.send(settings.KAFKA_TOPIC_NAME, key=b'fibonacci', value=json_string)
    producer.flush()

    detect_fib_numbers_for_users()


if __name__ == '__main__':
    print('Please wait\n')
    main()
    consumer.close()
